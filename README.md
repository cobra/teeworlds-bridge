# teeworlds-bridge

A Matrix\<-\>Teeworlds relay using `matrix-nio`

# Setup

First, set up the virtualenv and install the dependencies:

```
python3 -m venv venv
. venv/bin/activate
pip3 install -r requirements.txt
```

Be sure your Teeworlds configuration includes these statements:
```
ec_bindaddr 127.0.0.1
ec_port 8303
ec_password XXXXXXXX
```
It is very important that you are not binding on a public-facing IP.

Then, edit the config to fit your use-case.
```
cp config.py.example config.py
$EDITOR config.py
```

Finally, run the script:

```
python3 teeworlds-bridge.py
```
